#!/bin/bash

##################################################
###											   ###
### Glueing the openCV-based area measurements ###
### 	   and the summarizing Rscript   	   ###
###											   ###
##################################################

### help page
if [ $# == 3 ]; then
	### setting 3 input parameters:
	INPUT_DIR=$1		#directory where all the images are stored
	IMG_EXTENSION=$2	#extension name of the images, e.g. jpg and png
	MAC_OS=$3			#do you want to use the MacOS binary, i.e. 0 for NO and 1 for YES

	### create the output directory
	OUTPUT_DIR=$(echo $(date) | tr -d '[:space:]')
	OUTPUT_DIR=${INPUT_DIR}/OUTPUT_${OUTPUT_DIR}
	mkdir $OUTPUT_DIR

	### parallelize the openCV python script
	ls ${INPUT_DIR}/*.${IMG_EXTENSION} > images.list
	echo $OUTPUT_DIR > output.dir
	echo -e "#\!/bin/bash" > execute_mucilage_seed_areas_py.sh
	if [ $MAC_OS -eq 0 ]; then
		echo -e "python mucilage_seed_areas.py \$1 \$2 False" >> execute_mucilage_seed_areas_py.sh		
	else
		echo -e "./mucilage_seed_areas_macbin \$1 \$2 False" >> execute_mucilage_seed_areas_py.sh
	fi		
	
	sed -i 's/\\//g' execute_mucilage_seed_areas_py.sh
	chmod +x execute_mucilage_seed_areas_py.sh
	parallel ./execute_mucilage_seed_areas_py.sh {1} {2} ::: $(cat images.list) ::: $(cat output.dir)

	rm images.list output.dir execute_mucilage_seed_areas_py.sh

	### summarize per image
	./mucilage_seed_areas_summarize.r ${OUTPUT_DIR}

	### message
	echo -e "\n"
	echo -e "######################################################################################\n"
	echo -e "Done!"
	echo -e "\n"
	echo -e "Please find the results in:"
	echo -e "\t${INPUT_DIR}/OUTPUT_${OUTPUT_DIR}"
	echo -e "\t where the summarized output is called Seed-Mucilage-Area-OpenCV_<time-stamp>.csv"
	echo -e "#######################################################################################\n"
else
	echo -e "\n"
	echo -e "##########################################################################################"
	echo -e "Arabidopsis seed and seed mucilage area measurements with OpenCV3"
	echo -e "\t - first argument is the directory where all the input images are located"
	echo -e "\t - second argument is the extension of the input images (e.g. jpg and png)"
	echo -e "\t - third argument: Use the MacOSX binary? 1 for YES and 0 for NO.\n"
	echo -e "Example:"
	echo -e "\t ./mucilage_seed_areas_main.sh res/ jpg 0"
	echo -e "##########################################################################################\n"
fi
