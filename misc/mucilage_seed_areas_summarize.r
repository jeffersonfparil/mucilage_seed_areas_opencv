#!/usr/bin/env Rscript

#############################################
###										  ###
### Summarize openCV python script output ###
###										  ###
#############################################

### User input: directory where all the *-out.csv output of "mucilate_seed_area.py" are located
args = commandArgs(trailing=TRUE)
#args = c("/home/maheshika/hose/maheshika_seed_mucilage_20180907/res/NEW_20181130/OUT/")

### Set working directory there
setwd(args[1])

### List all *.out.csv files in that directory
files = system("ls *-out.csv", intern=TRUE)

### Summarize
ENTRY = gsub("-out.csv", "", files)
SEED_AREA = c()
MUCILAGE_AREA = c()
for (i in files){
	dat = read.csv(i)
	SEED_AREA = c(SEED_AREA, mean(dat$SEED_AREA, na.rm=TRUE))
	MUCILAGE_AREA = c(MUCILAGE_AREA, mean(dat$MUCILAGE_AREA, na.rm=TRUE))
}

OUT = data.frame(ENTRY=ENTRY, SEED_AREA=SEED_AREA, MUCILAGE_AREA=MUCILAGE_AREA)
OUT$MUCILAGE_OVER_SEED = OUT$MUCILAGE_AREA / OUT$SEED_AREA
write.csv(OUT, file=paste0("Seed-Mucilage-Area-OpenCV_", gsub(":", "-", gsub(" ", "", Sys.time())), ".csv"), row.names=FALSE)
