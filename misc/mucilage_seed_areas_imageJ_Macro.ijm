input_dir = getDirectory("Choose a Directory");
output_dir = getDirectory("Choose a Directory");
image_list = getFileList(input_dir);
setBatchMode(true);
for (i=0; i<image_list.length; i++) {
	showProgress(i+1, image_list.length);
	//seed + mucilage area
	open(input_dir + image_list[i]);
	print(input_dir + image_list[i]);
	run("Enhance Contrast", "saturated=0.35");
	run("Close");
	run("8-bit");
	run("Sharpen");
	run("Sharpen");
	run("Auto Threshold", "method=Triangle ignore_white");
	run("Analyze Particles...", "size=100-Infinity show=Outlines display clear include in_situ");
	saveAs("Results", output_dir + image_list[i] + " - totalArea.csv");
	saveAs("Jpeg", output_dir + image_list[i] + " - totalArea.jpg");
	close();
	//mucilage area alone
	open(input_dir + image_list[i]);
	print(input_dir + image_list[i]);
	run("Enhance Contrast", "saturated=0.35");
	run("Close");
	run("8-bit");
	run("Smooth");
	run("Smooth");
	run("Smooth");
	run("Auto Threshold", "method=Moments ignore_white");
	run("Analyze Particles...", "size=100-Infinity show=Outlines display clear include in_situ");
	saveAs("Results", output_dir + image_list[i] + " - seedArea.csv");
	saveAs("Jpeg", output_dir + image_list[i] + " - seedArea.jpg");
	close();
}