# mucilage_seed_areas_openCV

Measure Arabidopsis seed mucilage area

## MacOS executable guide (Requires MacOSX 10.13 High Sierra)

- Download `mucilage_seed_areas_macbin`
- Using Finder, navigate to the Downloads folder and rename the downloaded file from `mucilage_seed_areas_macbin.dms` to just `mucilage_seed_areas_macbin`
- Open the terminal:
    + Type: `cd Downloads`
    + Make the file executable: `chmod +x mucilage_seed_areas_macbin`
- `Right click` on the file and select `Open` and press `Open` on the pop-up window
- A video demonstration will show up below \(please allow a couple of minutes for the gif file to load\):

![Mac OS Guide:](res/guide_macos.gif)

# Let's make a draft of a methodology note

## Objectives:

1. develop a:
    - high-throughout
    - unsupervised
    - user-friendly
    - cross-platform
    - accurate
    method to measure *Arabidopsis thaliana* seed mucilage area from photographs
2.  which should be the better than existing imagej/Fiji-based alternatives in these respects, specifically compare with:
    - MuSeeQ [@Miart2018]
    - Base Fiji via analyze particles function for ROI's manually selected [@Ben-Tov2018]
    - Fiji-Weka
3.  repeatability of results should be high

## Introduction

- Seed coat descriptions seed coat essentiality to seed development, dormancy, germination etc...
- Mucilage mutants and related genes... cellulose stuff...
- Importance of measuring mucilage areas to characterise gene function...
- Current methods to measure mucilage area are dependent on the hugely popular and undoubtedly successful and reliable imageJ/Fiji software stack - but these require constant supervision i.e. corrections for non-uniform images which are high pervasive in most experiments and can be very challenging and labour-intensive for phenotyping large datasets. An automated method with minimal to no supervision is therefore required


## Methodology

- Here we developed an OpenCV implemented in Python 2.7 program with a user-friendly GUI for Windows, Linux and MacOS to measure *Arabidopsis thaliana* seed mucilage area


## Results

### Comparison with Base ImageJ/Fiji analyze particles function
### Comparison with Fiji-Weka
### Comparison with MuSeeQ
### Repeatability of our method

## Discussions

## References
