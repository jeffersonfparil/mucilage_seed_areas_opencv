#################################################################
###															  ###
### Estimate Arabidopsis thaliana seed and seed mucilage area ###
###															  ###
################################################################# 20180907

### import libraries
import cv2
import numpy as np
import matplotlib; matplotlib.use('Agg') #to prevent errors in MacOS compilation
from matplotlib import pyplot as plt
from scipy import stats
import sys, os, time
# import multiprocessing
# from functools import partial

### define the main function
def __MEASURE_SEED_MUCIALGE_AREAS__(PLOT, outdir, filename):
	img = cv2.imread(filename)				#load image
	nrows, ncols, nchannels = img.shape		#extract image dimensions
	# setWhite = 200						#background color (black:0 up to white:255) ### SUPERSEEDED
	MIN_SEED_SIZE = nrows*ncols/1000		#minimum seed size in square-pixels unit
	#just looking at the seed
	print(filename)
	IMAGE = np.copy(img)
	if PLOT==True: plt.imshow(cv2.cvtColor(IMAGE, cv2.COLOR_BGR2RGB)); plt.show()
	#white balance
	mean_column = np.mean(IMAGE[0:200,0:200], axis=0) #140:300, 2:220 # top-left corner!
	mean_row = np.mean(mean_column, axis=0)
	SET_WHITE = min(mean_row) #use the minimum pixel value across RGB as the white balancer
	CORRECTION_FACTOR = SET_WHITE/mean_row #setting the white styrofoam box color to 140-140-140
	IMAGE_COLCOR = np.uint8(CORRECTION_FACTOR*IMAGE)
	if PLOT==True: plt.imshow(cv2.cvtColor(IMAGE_COLCOR, cv2.COLOR_BGR2RGB)); plt.show()
	#remove gray background
	EPSILON=0.10
	TEST = ((1-(IMAGE_COLCOR[:,:, 0].astype('float')/IMAGE_COLCOR[:,:, 1]))<EPSILON) * ((1-(IMAGE_COLCOR[:,:, 0].astype('float')/IMAGE_COLCOR[:,:, 2]))<EPSILON) * ((1-(IMAGE_COLCOR[:,:, 1].astype('float')/IMAGE_COLCOR[:,:, 2]))<EPSILON)
	IMAGE_COLCOR[TEST,:] = [255,255,255]
	if PLOT==True: plt.imshow(cv2.cvtColor(IMAGE_COLCOR, cv2.COLOR_BGR2RGB)); plt.show()
	#convert to grayscale
	COLLAPSED_IMAGE = cv2.cvtColor(IMAGE_COLCOR, cv2.COLOR_BGR2GRAY)
	if PLOT==True: plt.imshow(COLLAPSED_IMAGE, cmap='gray'); plt.show()
	#erode to capture the faint edges of the mucilage
	kernel = np.ones((3,3), np.uint8)
	COLLAPSED_IMAGE = cv2.erode(COLLAPSED_IMAGE, kernel, iterations=1) #erode to increase area
	# COLLAPSED_IMAGE = cv2.dilate(COLLAPSED_IMAGE, kernel, iterations=1) #dilate to chop-off edges
	if PLOT==True: plt.imshow(COLLAPSED_IMAGE, cmap='gray'); plt.show()
	#identify seed+mucilage using the maximum pixel value as the object identification threshold
	THRESHOLD=COLLAPSED_IMAGE[COLLAPSED_IMAGE<255].max() ### max value refers to the lightest coloured pixel which should be the mucilage
	thresh, mask1 = cv2.threshold(COLLAPSED_IMAGE, THRESHOLD, 255, cv2.THRESH_BINARY_INV)
	if PLOT==True: plt.imshow(mask1, cmap='gray'); plt.show()
	#define contours of the seed+mucilage objects
	mask2, CONTOURS, hierarchy = cv2.findContours(mask1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	if PLOT==True: plt.imshow(mask2, cmap='gray'); plt.show()
	#remove background and show only the seeds for later use as the output image with labels
	IMAGE_EXTRACT = cv2.bitwise_and(IMAGE_COLCOR, IMAGE_COLCOR, mask=mask2)
	if PLOT==True: plt.imshow(cv2.cvtColor(IMAGE_EXTRACT, cv2.COLOR_BGR2RGB)); plt.show()
	counter=0
	AREAS = []
	IMG_OUT = np.copy(IMAGE_EXTRACT) #prepare output image for appending the labels
	for cnt in CONTOURS:
		area = cv2.contourArea(cnt)
		if area > MIN_SEED_SIZE:
			# test for seed overlap (exclude them)
			convex = cv2.contourArea(cv2.convexHull(cnt))
			solidity = float(area)/convex; #print(solidity)
			if solidity >= 0.90:
				counter = counter + 1
				seed_plus_mucilage_area = area
				[x,y,w,h] = cv2.boundingRect(cnt)
				#remove cropped seeds at the edge of the images
				if x!=0 and y!=0 and (x+w)!=ncols and (y+h)!=nrows:
					seed_mucilage = COLLAPSED_IMAGE[y:y+h, x:x+w]
					# plt.imshow(seed_mucilage, cmap='gray'); plt.show()
					mask_i = cv2.fillPoly(np.zeros((nrows, ncols), np.uint8), pts =[cnt], color=(255,255,255)) #draw and fill the contour on a black background
					# plt.imshow(mask_i, cmap='gray'); plt.show()
					mask_i = mask_i[y:y+h, x:x+w] #extract only the rectangular area containing the seed
					# plt.imshow(mask_i, cmap='gray'); plt.show()
					seed = cv2.bitwise_and(seed_mucilage, seed_mucilage, mask=mask_i) #extract the collapsed seed image without the other overlapping seeds unintentionally included when we cropped
					seed[seed==0] = 255		#convert black backgroud to white for contour identification
					# plt.imshow(seed, cmap='gray'); plt.show()
					threshold = stats.mode(seed[seed<255])[0][0]+20		#remove the background i.e. the white pixels and set the object identification threshold at mode + 20 pixel values
					# plt.hist(seed[seed<255].ravel(), 256); plt.show()
					thresh, mask_j = cv2.threshold(seed, threshold, 255, cv2.THRESH_BINARY_INV)		#identify objects based on threshold value
					# plt.imshow(mask_j, cmap='gray'); plt.show()
					mask_k, contours, hierarchy = cv2.findContours(mask_j, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)	#identify the contours of these objects
					# plt.imshow(mask_k, cmap='gray'); plt.show()
					seed_mucilage_extract = cv2.bitwise_and(IMAGE_COLCOR[y:y+h, x:x+w], IMAGE_COLCOR[y:y+h, x:x+w], mask=mask_i) #extract the colored image of the seed + mucilage with black background
					seed_extract = cv2.bitwise_and(IMAGE_COLCOR[y:y+h, x:x+w], IMAGE_COLCOR[y:y+h, x:x+w], mask=mask_k) #extract the colored image of the seed only with black background
					if PLOT==True: plt.imshow(cv2.cvtColor(seed_mucilage_extract, cv2.COLOR_BGR2RGB)); plt.show(); plt.imshow(cv2.cvtColor(seed_extract, cv2.COLOR_BGR2RGB)); plt.show()
					seed_area = 0
					for c in contours:
						seed_area = seed_area + cv2.contourArea(c)
						IMG_OUT[y:y+h, x:x+w] = cv2.drawContours(IMG_OUT[y:y+h, x:x+w], c, -1, (255,255,255), (nrows*ncols)/(640*480)) #draw the outline of the seed WITHOUT the mucilage with length thickness = image size in pixel-square divided by a smallish image of size 640x480
					mucilage_area = seed_plus_mucilage_area - seed_area
					AREAS.append((counter, seed_area, mucilage_area))
					IMG_OUT = cv2.drawContours(IMG_OUT, cnt, -1, (255,255,255), (nrows*ncols)/(640*480))  #draw the outline of the whole seed WITH the mucilage with length thickness = image size in pixel-square divided by a smallish image of size 640x480
					if PLOT==True: IMG_OUT = cv2.putText(IMG_OUT, str(counter) + ": S=" + str(seed_area) + ": M=" + str(mucilage_area), (x+int(w/2),y-10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), (nrows*ncols)/(4*640*480)) #print the areas
	if PLOT==True: plt.imshow(cv2.cvtColor(IMG_OUT, cv2.COLOR_BGR2RGB)); plt.show()
	#output files
	OUT = np.array(AREAS, dtype=[('ENTRY', int), ('SEED_AREA', 'f4'), ('MUCILAGE_AREA', 'f4')])
	file_out = filename.split("/")
	file_out = file_out[len(file_out)-1].split(".")[0]
	np.savetxt(fname=outdir + "/" + file_out + '-out.csv', X=OUT, delimiter=',', fmt=['%s', '%f', '%f'], header='ENTRY,SEED_AREA,MUCILAGE_AREA', comments='')
	cv2.imwrite(outdir + "/" + file_out + '-out.jpg', IMG_OUT)
	return(0)

###############
###			###
### EXECUTE ###
###			###
###############

### user input
# DIRECTORY="~/mucilage_seed_area/res"
# EXTENSION="jpg"
import Tkinter, tkFileDialog, tkSimpleDialog
root = Tkinter.Tk()
DIRECTORY = tkFileDialog.askdirectory()
EXTENSION = tkSimpleDialog.askstring("File extension name of the input images", "Enter image extension name:\n  e.g. jpg or png")
PLOT = False
root.withdraw()

filename_list = [DIRECTORY + "/" + f for f in os.listdir(DIRECTORY) if f.endswith('.' + EXTENSION)]
outdir = DIRECTORY + "/OUTPUT" + time.strftime("%Y-%m-%d-%H-%M")
os.mkdir(outdir)

### parallel processing
# PARALLEL = multiprocessing.Pool()
# PARALLEL_OUT = PARALLEL.map(partial(partial(__MEASURE_SEED_MUCIALGE_AREAS__, PLOT), outdir), filename_list)

### iterative processing instead (becuase parallel processing is not working on the mac vm)
for i in filename_list:
	__MEASURE_SEED_MUCIALGE_AREAS__(PLOT, outdir, i)

### summarize output into a single csv file
out_file_list = [outdir + "/" + f for f in os.listdir(outdir) if f.endswith('.' + 'csv')]
SUMMARY = []

for i in out_file_list:
	print(i)
	out_entry_name_immature = i.split("/")
	out_entry_name_rep = out_entry_name_immature[len(out_entry_name_immature)-1].split("-out.csv")[0]
	if len(out_entry_name_rep.split("_")[0:-1])>1:
		out_entry_name = "-".join(out_entry_name_rep.split("_")[0:-1])
	else:
		out_entry_name = out_entry_name_rep.split("_")[0]
	mean_seed_area = np.mean(np.loadtxt(i, delimiter=",", skiprows=1, usecols=1))
	mean_mucilage_area = np.mean(np.loadtxt(i, delimiter=",", skiprows=1, usecols=2))
	ratio = mean_mucilage_area / mean_seed_area
	try: #to prevent type error when there is only one sample (interestingly when ther is no sample len() works just fine)
		n = len(np.loadtxt(i, delimiter=",", skiprows=1, usecols=1))
	except:
		n = 1
	SUMMARY.append((out_entry_name, out_entry_name_rep, n, mean_seed_area, mean_mucilage_area, ratio))

OUT_SUMMARY = np.array(SUMMARY, dtype=[('ENTRY', np.unicode_, 16), ('ENTRY_REP', np.unicode_, 16), ('SAMPLE_SIZE', 'f4'), ('SEED_AREA', 'f4'), ('MUCILAGE_AREA', 'f4'), ('MUCILAGE_OVER_SEED', 'f4')])
OUT_SUMMARY.sort(order="ENTRY")
np.savetxt(fname=outdir + "/Seed-Mucilage-Area-OpenCV.csv", X=OUT_SUMMARY, delimiter=',', fmt=['%s', '%s', '%f', '%f', '%f', '%f'], header='ENTRY,ENTRY_REP,SAMPLE_SIZE,SEED_AREA,MUCILAGE_AREA,MUCILAGE_OVER_SEED', comments='')

# ### tests
# # __MEASURE_SEED_MUCIALGE_AREAS__(True, ".", "res/s1s2sep3_X4_004.jpg")
# __MEASURE_SEED_MUCIALGE_AREAS__(True, ".", "res/s1s2sep3_X4_002.jpg")

### getting the means per entry wherein an entry is identified by the string before the first underscore
from pandas import DataFrame, concat
df = DataFrame(OUT_SUMMARY)
MEAN = df.groupby("ENTRY").mean()
SUM = df.groupby("ENTRY").sum()
MIN = df.groupby("ENTRY").min()
MAX = df.groupby("ENTRY").max()
STD = df.groupby("ENTRY").std()
STE = df.groupby("ENTRY").sem()

AGGREGATE = concat([SUM.SAMPLE_SIZE, MEAN.MUCILAGE_OVER_SEED, MIN.MUCILAGE_OVER_SEED, MAX.MUCILAGE_OVER_SEED, STD.MUCILAGE_OVER_SEED, STE.MUCILAGE_OVER_SEED], axis=1)
AGGREGATE.columns = ["N", "MEAN", "MIN", "MAX", "STDEV", "STDERR"]
AGGREGATE.to_csv(outdir + "/Seed-Mucilage-Area-OpenCV_SUMMARY_STATS_PER_ENTRY.csv", header=True, sep=",")
